import React from 'react';
// Import the necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default function NaviBar() {
	return (
		<Navbar bg="light" expand="lg">
			<Navbar.Brand href="#home">Zuitter</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#courses">Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}

/*
================================
Class-Based Component Equivalent
================================
*/

/*

import React, { Component } from 'react';
// Import the necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

export default class NavBar extends Component {
    render() {
        return (
            <Navbar bg="light" expand="lg">
            	<Navbar.Brand href="#home">Zuitter</Navbar.Brand>
            	<Navbar.Toggle aria-controls="basic-navbar-nav" />
            	<Navbar.Collapse id="basic-navbar-nav">
            		<Nav className="ml-auto">
            			<Nav.Link href="#home">Home</Nav.Link>
            			<Nav.Link href="#courses">Courses</Nav.Link>
            		</Nav>
            	</Navbar.Collapse>
            </Navbar>
        )
    }
}

*/