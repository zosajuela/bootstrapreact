import React from 'react';
// import Jumbotron from 'react-bootstrap/Jumbotron';
// import Button from 'react-bootstrap/Button';
// Bootstrap grid system components
// import { Row } from 'react-bootstrap';
// import { Col } from 'react-bootstrap';
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

export default function Banner() {
	return (
		<Row>
		    <Col>
				<Jumbotron>
				    <h1>Zuitt Coding Bootcamp</h1>
				    <p>Opportunities for everyone, everywhere.</p>
				    <Button variant="primary">Enroll now!</Button>
				</Jumbotron>
		    </Col>
		</Row>
	)
}

/*
================================
Class-Based Component Equivalent
================================
*/

/* 

import React, { Component } from 'react';
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

export default class Banner extends Component {
    render() {
        return (
            <Row>
                <Col>
                    <Jumbotron>
                        <h1>Zuitt Coding Bootcamp</h1>
                        <p>Opportunities for everyone, everywhere.</p>
                        <Button variant="primary">Enroll now!</Button>
                    </Jumbotron>
                </Col>
            </Row>
        )
    }
}

*/