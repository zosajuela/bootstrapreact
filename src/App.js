import React from 'react';
import {useState} from 'react';
import { Container } from 'react-bootstrap';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import NaviBar from './components/NaviBar';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Error from './pages/Error'
import './App.css';
import {UserProvider} from './UserContext'
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

function App() {

	const [user, setUser] = useState({
		id: null, 
		firstName: null, 
		isAdmin: true
	})

	const unsetUser = () => {
		localStorage.clear()
		
		setUser({
			id: null,
			isAdmin: null,
			firstName: null,
			lastName: null
		})
	}

	return (
		<React.Fragment>
		<UserProvider value={{ user, setUser, unsetUser }}>
			<BrowserRouter>
				<NaviBar />
						<Container className="my-3">
					
							<Switch>
								<Route exact path="/" component={Home} />
								<Route exact path="/courses" component={Courses}/>
								<Route exact path="/register" component={Register} />
								<Route exact path="/login" component={Login} />
								<Route exact path="/register">
									{ (user.id !== null) ? <Redirect to="/courses" /> : <Register/>}
									
								</Route>
								<Route component={Error} />
							</Switch>

						</Container>
				</BrowserRouter>
			</UserProvider>
		</React.Fragment>
	);
}

export default App;

// WDC028 - S30 - Activity
	// <Courses />

// 	<Switch>
// 	<Route exact path="/" component={Home} />
// 	<Route exact path="/courses" render={()=> <Courses auth={auth} /> } />
// 	<Route exact path="/register" render={() => <Register auth={auth} />} />
// 	<Route exact path="/login" render={() => <Login auth={auth} setAuth={setAuth}/> }/>
// 	<Route component={Error} />
// </Switch>