import React from 'react';
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

export default function Error() {
	return (
		<Row>
		    <Col>
				<Jumbotron>
				    <h1>404 - Not found</h1>
				    <p>The page your are looking for cannot be found</p>
				    <Button variant="primary">Back home</Button>
				</Jumbotron>
		    </Col>
		</Row>
	)
}