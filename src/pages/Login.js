
import React from 'react';
import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Login(){

	/*
		objective - bind the state for email input field
		one way binding - added email to email input field
		two way binding - added setEmail to email input field onChange
	*/

    const { setUser } = useContext(UserContext);

		const [email, setEmail] = useState("Juan@email.com");
        const [password, setPassword] = useState("Juan");
        const [active, setActive] = useState(false)

        
    useEffect(() => {
        
        if(email.length === 0 || password.length === 0){
            setActive(true);
        } else{
            setActive(false);
        }
    },[email, password])

    const login = (e) => {
        e.preventDefault()
        alert("you have successfully login");
        setUser({
            id: 123,
            isAdmin: true,
            firstName: "Juan",
            lastName: "Smith"
        })
    }
	
	return( 
			<Form onSubmit={(e) => login(e)}>
			  <Form.Group controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email"
			    	value={email}
			    	onChange={(e)=> setEmail(e.target.value)} // we target the value of the input that fires the event onChange	
		    	/>
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                />
			  </Form.Group>
			  
			  <Button variant="primary" type="submit" disabled={active}>
			    Login
			  </Button>
			</Form>
		)
}