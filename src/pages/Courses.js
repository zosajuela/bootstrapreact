import React from 'react';
import Course from '../components/Course';
import coursesData from '../data/courses';

export default function Courses() {

	console.log(coursesData);

	const courses = coursesData.map(course => {
		return (
			<Course key={course.id} />
		)
	})

	return(
		<React.Fragment>
			{courses}
		</React.Fragment>
	)
}