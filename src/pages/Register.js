import React from 'react'
import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import {Redirect} from 'react-router-dom'

export default function Register(){

        const {user} = useContext(UserContext);


         const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");
        const [verifypassword, setVerifypassword] = useState("");
        const [active, setActive] = useState(false)
        let [color, setColor] = useState("primary");

    useEffect(() => {
        if(user.id !== null){
            <Redirect to="/courses" />
        }else{
           
        }
    })

    useEffect(() => {
        
        if(email.length === 0 || password.length === 0 || verifypassword === 0 || !password === verifypassword){
             setActive(true);
             setColor("warning");
        }else{
             setActive(false);
             setColor("primary");
        }
        
    },[email, password, verifypassword])

    const login = (e) => {
        e.preventDefault()
        alert("you have successfully logged in!")
    }
		console.log(active)


	return(
		<Form onSubmit={(e) => login(e)}>
		  <Form.Group controlId="formBasicEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
            type="email" 
            placeholder="Enter email"
            value={email}
            onChange={(e)=> setEmail(e.target.value)}/>
		    <Form.Text className="text-muted">
		      We'll never share your email with anyone else.
		    </Form.Text>
		  </Form.Group>

		  <Form.Group controlId="formBasicPassword">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
            type="password" 
            placeholder="Password" 
            value={password}
            onChange={(e) => setPassword(e.target.value)}/>
		  </Form.Group>


		  <Form.Group controlId="formBasicPassword">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control 
                type="password" 
                placeholder="verify password" 
                value={verifypassword}
                onChange={(e) => setVerifypassword(e.target.value)} />
		  </Form.Group>
		  
		  <Button variant={color} type="submit" disabled={active}> 
		    Register
		  </Button>
		</Form>
		)
}