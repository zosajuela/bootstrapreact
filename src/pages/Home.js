import React from 'react';
import Banner from '../components/Banner';
import Course from '../components/Course';
import Highlights from '../components/Highlights';

export default function Home() {
	return (
		<React.Fragment>
			<Banner />
			<Highlights />
			<Course />
		</React.Fragment>
	)
}